class IDIterator:
    def __init__(self, id): # CONSTRUCTOR
        self._id = id

    def __iter__(self): # GET ITERATOR
        return self

    def __next__(self): # GET NEXT ID
        if self._id == 999999999:
            raise StopIteration # stop iterator
        while self._id != 999999999 and not check_id_valid(self._id):
            self._id += 1
        self._id += 1 # for next time
        return self._id - 1 # the last id that was count


def sum_of_digits(number):
    """
    Function which sum the digit of the number
    :param number: int
    :return: sum of digits
    """
    return sum(map(int, str(number)))


def get_digit_mul_two_base_index(index_and_digit):
    """
    translate the digit base on even or odd index of digit
    :param index_and_digit: int
    :return: the reg digit or mul 2
    """
    i, digit_id = index_and_digit
    if i % 2 != 0:
        return int(digit_id) * 2
    return int(digit_id)


def check_id_valid(id_number):
    """
    Function which check if the id is valid
    :param id_number: int
    :return: True or False
    """
    id = list(map(get_digit_mul_two_base_index, enumerate(str(id_number))))
    return sum(map(lambda index_id_value: sum_of_digits(index_id_value) if index_id_value > 9 else index_id_value, id)) % 10 == 0


def id_generator(id):
    """
    Function generate valid ids
    :param id: int
    :return: generate first valid id
    """
    while id != 999999999:
        if check_id_valid(id):
            yield id
        id += 1



def main():
    count = 0 # init for task 2

    # task 1
    print(check_id_valid(123456782))

    # task 2, 3
    id = int(input("Enter ID: "))
    way_to_print = input("Generator or Iterator? (gen/it)? ") # task 6

    if way_to_print == 'it':
        itID = IDIterator(id)

        # generate ten ID's
        for id_val in itID:
            print(id_val)
            count += 1
            if count == 10:
                break

        print()
    # task 4, 5

    if way_to_print == 'gen':
        # generate ten ID's
        for id_val in id_generator(id):
            print(id_val)
            count += 1
            if count == 10:
                break





if __name__ == "__main__":
    main()
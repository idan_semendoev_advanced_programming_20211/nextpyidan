class MusicNotes:
    def __init__(self):
        self._notes = [('La', [55, 110, 220, 440, 880]),
                      ('Si', [61.74, 123.48, 246.96, 493.92, 987.84]),
                      ('Do', [65.41, 130.82, 261.64, 523.28, 1046.56]),
                      ('Re', [73.42, 146.84, 293.68, 587.36, 1174.72]),
                      ('Mi', [82.41, 164.82, 329.64, 659.28, 1318.56]),
                      ('Fa', [87.31, 174.62, 349.24, 698.48, 1396.96]),
                      ('Sol', [98, 196, 392, 784, 1568])]
        self._index = 0
        self._note_col = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._index == len(self._notes) and self._index > 0 and self._note_col == len(self._notes[self._index - 1][1]) - 1:
            raise StopIteration
        elif self._index == len(self._notes):
            self._index = 0
            self._note_col += 1
        freq = self._notes[self._index][1][self._note_col]
        self._index += 1
        return freq


notes_iter = iter(MusicNotes())
for freq in notes_iter:
    print(freq)
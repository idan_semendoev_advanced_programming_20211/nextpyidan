import winsound

freqs = {"la": 220,
         "si": 247,
         "do": 261,
         "re": 293,
         "mi": 329,
         "fa": 349,
         "sol": 392,
         }

notes = "500-sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol"
notes = [[int(couple.split('-')[0]), couple.split('-')[1]] for couple in notes.split(',')]

for noteIt in notes:
    frequency = freqs[noteIt[1]]
    duration = noteIt[0]
    winsound.Beep(frequency, duration)


def is_prime(n):
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


def first_prime_over(n):
    num = n + 1
    while True:
        if is_prime(num):
            return num
        num += 1


print(first_prime_over(1000000))

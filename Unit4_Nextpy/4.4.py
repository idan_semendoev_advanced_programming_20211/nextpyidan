def gen_secs():
    for sec in range(0, 60):
        yield sec


def gen_minutes():
    for min in range(0, 60):
        yield min


def gen_hours():
    for hour in range(0, 24):
        yield hour


def gen_time():
    for h in gen_hours():
        for m in gen_minutes():
            for s in gen_secs():
                yield "%02d:%02d:%02d" % (h, m, s)


def gen_years(start=2019):
    end_year = 2023
    for year in range(start, end_year):
        yield year


def gen_months():
    for month in range(1, 13):
        yield month


def create_gen_days(end):
    for day in range(1, end + 1):
        yield day


def gen_days(month, leap_year=True):
    match month:
        case 1:
            return create_gen_days(31)
        case 2:
            # check leap year
            if leap_year:
                return create_gen_days(29)
            else:
                return create_gen_days(28)
        case 3:
            return create_gen_days(31)
        case 4:
            return create_gen_days(30)
        case 5:
            return create_gen_days(31)
        case 6:
            return create_gen_days(30)
        case 7:
            return create_gen_days(31)
        case 8:
            return create_gen_days(31)
        case 9:
            return create_gen_days(30)
        case 10:
            return create_gen_days(31)
        case 11:
            return create_gen_days(30)
        case 12:
            return create_gen_days(31)


def gen_date():
    for y in gen_years(2019):
        for m in gen_months():
            leap = False
            if y % 400 == 0 or (y % 4 == 0 and y % 100 != 0):
                leap = True
            for d in gen_days(m, leap):
                for time in gen_time():
                    yield "%02d/%02d/%04d " % (d, m, y) + time


def main():
    # declare
    num = 0

    # print date
    for date in gen_date():
        if num > 0 and num % 1000000 == 0:
            print(date)
        num += 1


if __name__ == "__main__":
    main()


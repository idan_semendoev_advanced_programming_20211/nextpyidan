def get_fibo():
    curr = 0
    after = 1
    while True:
        yield curr
        curr, after = after, curr + after


fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))

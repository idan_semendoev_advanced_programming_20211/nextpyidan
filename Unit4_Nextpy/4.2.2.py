def parse_ranges(ranges_string):
    first_generator =  ([ranges.split('-')[0], ranges.split('-')[1]] for ranges in ranges_string.split(','))
    sec_generator = (num for start, stop in first_generator for num in range(int(start), int(stop) + 1))
    return sec_generator


print(list(parse_ranges("1-2,4-4,8-10")))

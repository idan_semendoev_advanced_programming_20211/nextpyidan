def read_file(file_name):
    print("__CONTENT_START__")
    try:
        file = open(file_name, 'r')
    except:
        print("__NO_SUCH_FILE__")
    else:
        print(file.read())
        file.close()
    finally:
        print("__CONTENT_END__")


read_file("file.txt")

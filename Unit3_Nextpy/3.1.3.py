def stop_iteration():
    it = iter([])
    next_value = next(it)

def zero_division():
    res = 4 / 0


def assertion_error():
    my_list = [1, 2, 3]
    #assert len(my_list) > 5, "List length is not greater than 5"

def import_error():
    import not_a_module_exists

def key_error():
    my_dict = {"a": 1, "b": 2, "c": 3}
    value = my_dict["d"]


def syntax_error():
    pass # = True



def indentation_error():
    if True:
        print('none')
         #print('?')



def type_error():
    y = 3
    #print(y[3])

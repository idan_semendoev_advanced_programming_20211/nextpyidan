import string
class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, invalid_character, index):
        self._invalid_character = invalid_character
        self._index = index

    def __str__(self):
        return "The username contains an illegal character '" + self._invalid_character + "' at index " + self._index


class UsernameTooShort(Exception):
    def __str__(self):
        return "Username len too short. len is under 3."


class UsernameTooLong(Exception):
    def __str__(self):
        return "Username len too long. len is above 16."


class PasswordMissingCharacter(Exception):
    def __init__(self, error_type):
        self._error_type = error_type

    def __str__(self):
        return "The password is missing a character (" + self._error_type + ")"


class PasswordTooShort(Exception):
    def __str__(self):
        return "Password len too short. len is under 8."


class PasswordTooLong(Exception):
    def __str__(self):
        return "Password len too long. len is above 40."


def check_input(username, password):
    try:
        invalid_char_list = list(filter(lambda char: not (char.isalnum() or char == '_'), username))
        if len(username) < 3:
            raise UsernameTooShort()
        elif len(username) > 16:
            raise UsernameTooLong()
        elif len(invalid_char_list) > 0:
            raise UsernameContainsIllegalCharacter(invalid_char_list[0], str(username.find(invalid_char_list[0])))
        elif len(password) < 8:
            raise PasswordTooShort
        elif len(password) > 40:
            raise PasswordTooLong
        elif not any(char.isupper() for char in password):
            raise PasswordMissingCharacter("Uppercase")
        elif not any(char.islower() for char in password):
            raise PasswordMissingCharacter("Lowercase")
        elif not any(char.isdigit() for char in password):
            raise PasswordMissingCharacter("Digit")
        elif not any(char in string.punctuation for char in password):
            raise PasswordMissingCharacter("Special")
    except Exception as e:
        print(e)
    else:
        print("OK")


def main():
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")


if __name__ == "__main__":
    main()
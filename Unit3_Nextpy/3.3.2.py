class UnderAge(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Guest age is under 18.\nAge Curr: %d\nMore %d Years left" % (self._arg, 18 - self._arg)

    def get_arg(self):
        return self._arg


def send_invitation(name, age):
    if int(age) < 18:
        raise UnderAge(age)
    else:
        print("You should send an invite to " + name)

try:
    send_invitation("idan", 17)
except UnderAge as e:
    print(e)
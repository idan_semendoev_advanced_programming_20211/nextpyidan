def combine_coins(coin, numbers):
    return ", ".join(list(map(lambda item: coin + str(item), numbers)))


print(combine_coins('$', range(5)))

def only_mod_four(x):
    return x % 4 == 0

def four_dividers(number):
    return list(filter(only_mod_four, range(1, number)))


print(four_dividers(9))

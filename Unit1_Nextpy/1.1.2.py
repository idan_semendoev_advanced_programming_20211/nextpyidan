import functools


def double_char(str, char_in_str):
    str += char_in_str * 2
    return str

def double_letter(my_str):
    return functools.reduce(double_char, my_str, "")


print(double_letter("we are the champions!"))
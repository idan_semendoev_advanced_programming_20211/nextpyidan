import functools


def main():
    # task 1
    with open("names.txt", 'r') as name_file:
        print(sorted(name_file.read().split('\n'), key=lambda str: len(str))[-1])

    # task 2
    with open("names.txt", 'r') as name_file:
        print(sum(map(lambda str: len(str), name_file.read().split('\n'))))

    # task 3
    with open("names.txt", 'r') as name_file:
        names = sorted(name_file.read().split('\n'), key=lambda str: len(str))
        print("\n".join([name for name in names if len(name) == len(names[0])]))

    # task 4
    with open("names.txt", 'r') as name_file, open("name_length.txt", 'w') as write_to_file:
        write_to_file.write('\n'.join(list(map(lambda string: str(len(string)), name_file.read().split('\n')))))

    # task 5
    with open("names.txt", 'r') as name_file:
        lenToFind = int(input("Enter name length: "))
        print('\n'.join(list(filter(lambda string: len(string) == lenToFind, name_file.read().split('\n')))))

if __name__ == "__main__":
    main()

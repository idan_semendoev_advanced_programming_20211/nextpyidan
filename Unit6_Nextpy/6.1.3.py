from tkinter import *
from PIL import Image, ImageTk


def show_image():
    if not hasattr(show_image, "already_shown"):
        img = Image.open("funny.jpg")

        max_size = (400, 400)
        img.thumbnail(max_size, Image.ANTIALIAS)

        photo = ImageTk.PhotoImage(img)

        # create label to display image
        img_label = Label(root, image=photo)
        img_label.image = photo
        img_label.pack()
        
        show_image.already_shown = True

root = Tk()

root.geometry("600x400")

Label(root, text="What's my favorite video?", font=('Times 18'), width=20, height=0).pack()
Button(root, text="click to find out!", font=('Times 15'), width=20, height=1, command=show_image).pack()
PhotoImage()
root.mainloop()
import pyttsx3

def main():
    sentence = "first time i'm using a package in next.py course"

    engine = pyttsx3.init()
    engine.say(sentence)

    # run
    engine.runAndWait()


if __name__ == "__main__":
    main()

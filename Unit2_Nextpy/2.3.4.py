class Pixel:
    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y):
        self._x = x
        self._y = y

    def set_grayscale(self):
        avg = (self._red + self._green + self._blue) // 3
        self._red = avg
        self._green = avg
        self._blue = avg

    def print_pixel_info(self):
        rgb = [self._red, self._green, self._blue]
        color_to_print = ''
        if rgb.count(0) == 2:
            if self._red > 50:
                color_to_print = "Red"
            if self._green > 50:
                color_to_print = "Green"
            if self._blue > 50:
                color_to_print = "Blue"

        print("X: %d, Y: %d, Color: (%d, %d, %d) %s" %
              (self._x, self._y, self._red, self._green, self._blue, color_to_print))


def main():
    p = Pixel(5, 6, 250)
    p.print_pixel_info()
    p.set_grayscale()
    p.print_pixel_info()



if __name__ == "__main__":
    main()
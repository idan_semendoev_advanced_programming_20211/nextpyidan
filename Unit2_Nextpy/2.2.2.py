class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age


def main():
    dog_1 = Dog('chipopo', 7)
    dog_2 = Dog('lolo', 3)
    dog_1.birthday()
    print(dog_1.age, dog_2.age)


if __name__ == "__main__":
    main()
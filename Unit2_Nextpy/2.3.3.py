class Dog:
    counter = 0
    def __init__(self, age, name="Octavio"):
        self._name = name
        self._age = age
        Dog.counter += 1

    def set_name(self, name):
        self._name = name

    def birthday(self):
        self._age += 1

    def get_age(self):
        return self._age

    def get_name(self):
        return self._name

    def count_animals(self):
        print(Dog.counter)


def main():
    dog_1 = Dog(7, 'chipopo')
    dog_2 = Dog(3)

    print(dog_1.get_name(), dog_2.get_name())

    dog_2.set_name('lolo')
    print(dog_2.get_name())

    dog_1.count_animals()


if __name__ == "__main__":
    main()
class BigThing:
    def __init__(self, thing):
        self._thing = thing

    def size(self):
        if isinstance(self._thing, int):
            return self._thing
        else:
            return len(self._thing)


class BigCat(BigThing):
    def __init__(self, name, weight):
        super().__init__(name)
        self._weight = weight

    def size(self):
        if self._weight > 20:
            return "Very Fat"
        elif self._weight > 15:
            return "Fat"
        else:
            return "OK"


cutie = BigCat("mitzy", 22)
print(cutie.size())